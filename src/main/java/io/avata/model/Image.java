package io.avata.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.commons.io.IOUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Paul Daniel Iway
 */
@JsonAutoDetect(getterVisibility = Visibility.NONE)
public class Image {

  private final File imageFile;
  private final InputStream inputStream;

  private final int width;
  private final int height;
  private final String format;
  private final String mimeType;
  private final boolean isTransparent;
  private final boolean isProgressive;


  protected Image(File imageFile,
                  InputStream is,
                  int width,
                  int height,
                  String format,
                  String mimeType,
                  boolean isTransparent,
                  boolean isProgressive) {
    this.width = width;
    this.height = height;
    this.format = format;
    this.mimeType = mimeType;
    this.isTransparent = isTransparent;
    this.isProgressive = isProgressive;
    this.inputStream = is;
    this.imageFile = imageFile;
  }

  public File getImageFile() {
    return imageFile;
  }

  public boolean exists() {return imageFile.exists();}

  @JsonSerialize
  @JsonProperty("length")
  public long length() {return imageFile.length();}


  @JsonSerialize
  @JsonProperty("name")
  public String getName() {return imageFile.getName();}

  public String getPath() {return imageFile.getPath();}

  public boolean canRead() {return imageFile.canRead();}

  public boolean canWrite() {return imageFile.canWrite();}

  @JsonSerialize
  @JsonProperty("lastModified")
  public long lastModified() {return imageFile.lastModified();}

  public boolean delete() {return imageFile.delete();}

  @JsonSerialize
  @JsonProperty("width")
  public int getWidth() {
    return this.width;
  }

  @JsonSerialize
  @JsonProperty("height")
  public int getHeight() {
    return this.height;
  }

  @JsonSerialize
  @JsonProperty("format")
  public String getFormat() {
    return format;
  }

  @JsonSerialize
  @JsonProperty("mimeType")
  public String getMimeType() {
    return mimeType;
  }

  @JsonSerialize
  @JsonProperty("transparent")
  public boolean isTransparent() {
    return isTransparent;
  }

  @JsonSerialize
  @JsonProperty("progressive")
  public boolean isProgressive() {
    return isProgressive;
  }

  public byte[] getData() throws IOException {
    return IOUtils.toByteArray(this.inputStream);
  }

  public BufferedImage getBufferedImage() throws IOException {
    return ImageIO.read(this.inputStream);
  }
}