package io.avata.model;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Paul Daniel Iway
 */
public class ImageBuilder {


  private File file;
  private InputStream is;
  private String filename;

  public ImageBuilder(String filename) {
    this.filename = filename;
  }

  public ImageBuilder(File file, InputStream is) {
    if (file.isDirectory()) {
      throw new IllegalArgumentException("Expected file but got directory:" + file.getPath());
    }
    this.file = file;
    this.is = is;
  }

  public Image build() throws IOException, ImageReadException {
    if (this.file == null) {
      this.file = new File(this.filename);
    }

    if (this.is == null) {
      this.is = new FileInputStream(file);
    }

    ImageInfo info = Imaging.getImageInfo(this.file);

    Image image = new Image(this.file,
                            this.is,
                            info.getWidth(),
                            info.getHeight(),
                            info.getFormat().getName(),
                            info.getMimeType(),
                            info.isTransparent(),
                            info.isProgressive()
    );
    return image;
  }
}
