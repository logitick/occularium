package io.avata;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json
    .MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation
    .ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation
    .WebMvcConfigurerAdapter;

import java.util.List;

/**
 * @author Paul Daniel Iway
 */

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    //configurer.favorPathExtension(false);
  }

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>>
                                               converters) {
    super.configureMessageConverters(converters);
    converters.add(new BufferedImageHttpMessageConverter());
    converters.add(new ByteArrayHttpMessageConverter());
    converters.add(new MappingJackson2HttpMessageConverter());
  }
}
