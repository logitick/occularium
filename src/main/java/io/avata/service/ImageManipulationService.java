package io.avata.service;

import io.avata.processor.ImageProcessor;
import io.avata.processor.ProcessorEngine;
import io.avata.processor.commands.ImageProcessorCommand;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Paul Daniel Iway
 */
@Component
public class ImageManipulationService {

  @Autowired
  @Qualifier("defaultEngine")
  private ProcessorEngine engine;


  private static final Logger logger =
      (Logger) LoggerFactory.getLogger(ImageManipulationService.class);

  private ImageProcessor getImageProcessor(){
    return new ImageProcessor(this.engine);
  }

  public void setEngine(ProcessorEngine engine) {
    this.engine = engine;
  }

  public ImageProcessor fromMap(Map<String, String> query){

    ImageProcessor processor =this.getImageProcessor();


    int width = Integer.parseInt(query.getOrDefault("w", "0"));
    int height = Integer.parseInt(query.getOrDefault("h", "0"));
    query.remove("w");
    query.remove("h");

    int x = Integer.parseInt(query.getOrDefault("x", "1"));
    int y = Integer.parseInt(query.getOrDefault("y", "1"));
    query.remove("x");
    query.remove("y");

    query.forEach((name, value) -> {
      ImageProcessorCommand command = null;

      switch (name) {
        case "scale":

          if (NumberUtils.isNumber(value)) {
            command = ImageProcessor.resize(Double.parseDouble(value));
          } else {
            command = ImageProcessor.resize(width, height);
          }

          break;
        case "crop":
          command = ImageProcessor.crop(width, height, x, y);
          break;
      }
      if (command != null && !processor.commandExists(command)) {
        processor.addCommand(command);
      }
    });
    return processor;
  }

}
