package io.avata.service;

import io.avata.model.Image;
import io.avata.model.ImageBuilder;
import io.avata.processor.ImageProcessor;
import io.avata.utils.ImageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Paul Daniel Iway
 */
@Component("imageService")
public class ImageService {

  private String imageBasePath;

  @Autowired
  private ImageManipulationService manipulator;


  @Autowired
  private ImageUtil imageUtil;

  @Autowired
  public ImageService(@Value("${image.path}") String imageRoot) {
    this.imageBasePath = imageRoot;
  }

  public String getImagePath(String filename){
    return this.imageBasePath.concat(filename);
  }

  public ImageBuilder getImageBuilder(String filename) {
    ImageBuilder builder = new ImageBuilder(this.getImagePath(filename));
    return builder;
  }

  public byte[] fromMap(Map<String, String[]> parameterMap, Image image) throws IOException {
    return this.imageUtil.toByteArray(this.bufferedImageFromMap(parameterMap,
                                                                image), image.getFormat());
  }

  public BufferedImage bufferedImageFromMap(Map<String, String[]> parameterMap, Image image) throws IOException {
    if (parameterMap.isEmpty()) {
      return image.getBufferedImage();
    }
    ImageProcessor processor = this.manipulator.fromMap(this.flattenMap(parameterMap));
    return processor.process(image.getBufferedImage());
  }

  /**
   *
   * @param parameterMap
   * @return the flattened hash map of a query string
   */
  public LinkedHashMap<String, String> flattenMap(Map<String, String[]> parameterMap) {
    LinkedHashMap<String, String> flatMap = new LinkedHashMap<>();
    parameterMap.forEach((name, value) -> flatMap.put(name, value[0]));
    return flatMap;
  }

  public void setManipulator(ImageManipulationService manipulator) {
    this.manipulator = manipulator;
  }

  public void setImageUtil(ImageUtil imageUtil) {
    this.imageUtil = imageUtil;
  }
}
