package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

import java.awt.image.BufferedImage;

/**
 * @author Paul Daniel Iway
 */
public class Crop implements ImageProcessorCommand{

  private ProcessorEngine engine;

  private int width;
  private int height;
  private int x;
  private int y;

  public Crop(int width, int height, int x, int y) {

    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
  }

  @Override
  public BufferedImage execute(BufferedImage image) throws ProcessorEngineNotSetException {
    return this.engine.crop(image, this.width, this.height, this.x, this.y);
  }

  @Override
  public void setEngine(ProcessorEngine engine) {
    this.engine = engine;
  }

  @Override
  public ProcessorEngine getEngine() {
    return this.engine;
  }
}
