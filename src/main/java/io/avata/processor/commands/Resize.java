package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

import java.awt.image.BufferedImage;
import java.util.OptionalDouble;

/**
 * @author Paul Daniel Iway
 */
public class Resize extends ModeableCommand {


  private ProcessorEngine engine;
  private int width;
  private int height;
  private OptionalDouble percentage = OptionalDouble.empty();

  public Resize(final int width, final int height) {
    this.width = width;
    this.height = height;
  }

  public Resize(final double percentage) {
    this.percentage = OptionalDouble.of(percentage);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public BufferedImage execute(BufferedImage image) throws ProcessorEngineNotSetException{
    this.checkEngine();
    ProcessorEngine.Mode mode = ProcessorEngine.Mode.EXPLICIT;
    if (this.percentage.isPresent()) {
      this.width = (int) (image.getWidth() * this.percentage.getAsDouble());
      this.height = (int) (image.getHeight() * this.percentage.getAsDouble());
      mode = ProcessorEngine.Mode.IMPLICIT;

    }

    return engine.resize(image, this.width, this.height, mode);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void setEngine(ProcessorEngine engine) {
    this.engine = engine;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ProcessorEngine getEngine() {
    return this.engine;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) { return true; }
    if (!(o instanceof Resize)) { return false; }

    Resize resize = (Resize) o;
    if (percentage != resize.percentage) { return false; }
    if (width != resize.width) { return false; }
    return height == resize.height;

  }
}
