package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

/**
 * Thrown when trying to execute a {@link ImageProcessorCommand} when it has
 * no {@link ProcessorEngine} set.
 * @author Paul Daniel Iway
 */
public class ProcessorEngineNotSetException extends Exception {
}
