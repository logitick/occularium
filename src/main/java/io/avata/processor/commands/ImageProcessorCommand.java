package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

import java.awt.image.BufferedImage;

/**
 * A single command to manipulate an image.
 * @author Paul Daniel Iway
 */
public interface ImageProcessorCommand {

  /**
   * Executes the command instructions.
   * @param image The image to be manipulated.
   * @return The manipulated image
   * @throws ProcessorEngineNotSetException When no {@link ProcessorEngine}
   * has been set.
   */
  BufferedImage execute(BufferedImage image) throws ProcessorEngineNotSetException;

  /**
   * Sets the engine that alters the image.
   * @param engine
   */
  void setEngine(ProcessorEngine engine);

  /**
   * Gets the engine instance for this command.
   * @return
   */
  ProcessorEngine getEngine();

  /**
   * Does nothing but throw exception when the engine is not set.
   * @throws ProcessorEngineNotSetException
   */
  default void checkEngine() throws ProcessorEngineNotSetException {
    if(this.getEngine() == null) {
      throw new ProcessorEngineNotSetException();
    }
  }

  @Override
  boolean equals(Object o);
}
