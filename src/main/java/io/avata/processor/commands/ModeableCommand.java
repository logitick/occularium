package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

/**
 * @author Paul Daniel Iway
 */
public abstract class ModeableCommand implements ImageProcessorCommand {

  protected ProcessorEngine.Mode mode = ProcessorEngine.Mode.IMPLICIT;

  public void setMode(ProcessorEngine.Mode m) {
    this.mode = m;
  }

  public ProcessorEngine.Mode getMode() {
    return this.mode;
  }
}
