package io.avata.processor;

import java.awt.image.BufferedImage;

/**
 * Basically an adapter for image manipulation libraries.
 * @author Paul Daniel Iway
 */
public interface ProcessorEngine {

  enum Mode {
    IMPLICIT, EXPLICIT
  }
  /**
   * Resize an image
   * @param image The image that will be resized.
   * @param width The new width of the resized image.
   * @param height The new height of the resized image.
   * @param mode the resize mode
   * @return resized image
   */
  BufferedImage resize(BufferedImage image, int width, int height, Mode mode);

  BufferedImage crop(BufferedImage image, int width, int height, int x, int y);
}
