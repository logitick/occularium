package io.avata.processor;

import org.imgscalr.Scalr;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;

/**
 * Image Manipulation using ImgScalr.
 *
 * @author Paul Daniel Iway
 */
@Component(value = "defaultEngine")
public class ImgScalrEngine implements ProcessorEngine {

  private BufferedImage resize(BufferedImage image, int width, int height, Scalr.Mode m) {
    return Scalr.resize(image, m, width, height);
  }

  @Override
  public BufferedImage resize(BufferedImage image, int width, int height, Mode mode) {

    Scalr.Mode m = Scalr.Mode.AUTOMATIC;
    switch (mode) {
      case IMPLICIT:
        // width must not have been specified in query string so fit to height
        if (width == 0) {
          m = Scalr.Mode.FIT_TO_HEIGHT;
        }

        if (height ==0) {
          m = Scalr.Mode.FIT_TO_WIDTH;
        }

        break;
      case EXPLICIT:
        m = Scalr.Mode.FIT_EXACT;
        break;
    }
    return this.resize(image, width, height, m);
  }

  @Override
  public BufferedImage crop(BufferedImage image, int width, int height, int x, int y) {
    return Scalr.crop(image, x, y, width, height);
  }
}
