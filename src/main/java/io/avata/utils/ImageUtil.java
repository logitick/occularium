package io.avata.utils;

import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author Paul Daniel Iway
 */
@Component("imageUtil")
public class ImageUtil {
  public byte[] toByteArray(BufferedImage image, String format) throws IOException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    ImageIO.write(image, format, os);
    return os.toByteArray();
  }
}
