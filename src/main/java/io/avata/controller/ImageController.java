package io.avata.controller;


import io.avata.controller.response.InternalErrorException;
import io.avata.controller.response.NotFoundException;
import io.avata.model.Image;
import io.avata.model.ImageBuilder;
import io.avata.service.ImageService;
import org.apache.commons.imaging.ImageReadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Paul Daniel Iway
 */
@RestController
public class ImageController {

  private static final Logger logger =
      (Logger) LoggerFactory.getLogger(ImageController.class);

  @Autowired
  private ImageService imageService;

  @RequestMapping(value = "/image/{filename:.+}.json", produces = "application/json; charset=utf-8", consumes = "application/json")
  @ResponseBody
  public Image meta(@PathVariable String filename) {
    try {
      return this.imageService.getImageBuilder(filename).build();
    } catch (FileNotFoundException e) {
        throw new NotFoundException();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ImageReadException e) {
      e.printStackTrace();
    }

    throw new InternalErrorException();
  }

  public byte[] obj(@PathVariable String filename, HttpServletRequest request) {
    try {
      ImageBuilder builder = this.imageService.getImageBuilder(filename);
      return this.imageService.fromMap(request.getParameterMap(), builder.build());
    } catch (FileNotFoundException e) {
      logger.info("Image not found", e);
      throw new NotFoundException();
    } catch (IOException e) {
      logger.error("IOException", e);
    } catch (ImageReadException e) {
      logger.error("ImageReadException", e);
    }
    throw new InternalErrorException();
  }

  @RequestMapping(value = "/image/{filename:.+}")
  @ResponseBody
  public BufferedImage image(@PathVariable String filename, HttpServletRequest request) {
    try {
      ImageBuilder builder = this.imageService.getImageBuilder(filename);
      BufferedImageHttpMessageConverter c = new BufferedImageHttpMessageConverter();
      return this.imageService.bufferedImageFromMap(request.getParameterMap(),
                                                    builder.build());
    } catch (FileNotFoundException e) {
      logger.info("Image not found", e);
      throw new NotFoundException();
    } catch (IOException e) {
      logger.error("IOException", e);
    } catch (ImageReadException e) {
      logger.error("ImageReadException", e);
    }
    throw new InternalErrorException();
  }
}
