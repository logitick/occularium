package io.avata.service;

import io.avata.processor.ImageProcessor;
import io.avata.processor.ProcessorEngine;
import io.avata.processor.commands.Resize;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author Paul Daniel Iway
 */
public class ImageManipulationServiceTest {

  private ImageManipulationService service;
  private ProcessorEngine engine;

  @Before
  public void setUp() {
    this.engine = mock(ProcessorEngine.class);
    this.service = new ImageManipulationService();
    this.service.setEngine(this.engine);
  }

  @Test
  public void testFromMap() {
    HashMap<String, String> map = new HashMap<>();
    map.put("scale", "custom");
    map.put("h", "10");
    map.put("w", "10");
    ImageProcessor processor = this.service.fromMap(map);
    assertTrue(processor.commandExists(new Resize(10, 10)));
  }
}