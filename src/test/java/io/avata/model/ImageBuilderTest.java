package io.avata.model;

import org.junit.Test;

import java.io.File;

/**
 * @author Paul Daniel Iway
 */
public class ImageBuilderTest {


  @Test(expected = IllegalArgumentException.class)
  public void testBuilderWithDirectoryFile() {
    new ImageBuilder(new File("/"), null);
  }
}